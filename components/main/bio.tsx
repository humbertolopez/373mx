"use client";

import { useQuery } from "@tanstack/react-query";
import { fetchSinglePage } from "@/data/pages/pagesQueries";

const TheBio = () => {
  const params = {
    slug: "bio",
  };
  const { data, isLoading } = useQuery({
    queryKey: ["bio"],
    queryFn: () => fetchSinglePage(params),
  });

  if (isLoading)
    <section>
      <p>Loading...</p>
    </section>;

  return (
    <section className="w-full border-b px-10">
      <div className="container mx-auto urbanist font-medium py-20">
        {data && (
          <div
            dangerouslySetInnerHTML={{ __html: data[0].content.rendered }}
          ></div>
        )}
      </div>
    </section>
  );
};

export default TheBio;
