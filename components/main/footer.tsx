"use client";

const TheFooter = () => {
  return (
    <footer className="w-full border-t mt-10 pb-10 pt-10 px-8">
      <section className="xl:container mx-auto">
        <ul>
          <li>
            <a href="https://twitter.com/sachiel" className="underlined_hover">
              X
            </a>
          </li>
          <li>
            <a
              href="https://www.instagram.com/lopez_humberto/"
              className="underlined_hover"
            >
              Instagram
            </a>
          </li>
          <li>
            <a
              href="https://www.threads.net/@lopez_humberto"
              className="underlined_hover"
            >
              Threads
            </a>
          </li>
          <li>
            <a
              href="https://www.linkedin.com/in/humbertolopez/"
              className="underlined_hover"
            >
              LinkedIn
            </a>
          </li>
        </ul>
      </section>
    </footer>
  );
};

export default TheFooter;
