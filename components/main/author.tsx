import { useQuery } from "@tanstack/react-query";
import { fetchAuthor } from "@/data/authors/authorsQueries";

type Props = {
  authorId: string;
};

export default function TheAuthor({ authorId }: Props) {
  const { data, isLoading } = useQuery({
    queryKey: ["author"],
    queryFn: () => fetchAuthor(authorId),
  });
  return (
    data && <div className="josefinsans uppercase text-center">{data.name}</div>
  );
}
