"use client";

import { fetchSinglePost } from "@/data/posts/postsQueries";
import { useQuery } from "@tanstack/react-query";
import TheAuthor from "@/components/main/author";
import dayjs from "dayjs";

type Props = {
  slug: string;
};

export default function SinglePost({ slug }: Props) {
  const { data, isLoading } = useQuery({
    queryKey: ["post", slug[0]],
    queryFn: () => fetchSinglePost(slug[0]),
  });
  if (isLoading) return <article>Is loading...</article>;
  return (
    <section id="single-post" className="container max-w-screen-lg mx-auto">
      {data.length ? (
        <article key={data[0].id}>
          <h1 className="text-5xl text-center leading-snug mb-10 mt-10 urbanist font-medium">
            {data[0].title.rendered}
          </h1>
          <TheAuthor authorId={data[0].author}></TheAuthor>
          <div className="josefinsans text-center">
            {dayjs(data[0].date).format("MM/DD/YYYY")}
          </div>
          <div
            dangerouslySetInnerHTML={{ __html: data[0].content.rendered }}
          ></div>
        </article>
      ) : (
        <p>404!</p>
      )}
    </section>
  );
}
