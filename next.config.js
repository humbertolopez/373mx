/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    api_url: "https://www.api.373.mx/wp-json/wp/v2",
    base_url: "http://localhost:3000",
  },
};

module.exports = nextConfig;
