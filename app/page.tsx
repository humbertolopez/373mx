import {
  dehydrate,
  HydrationBoundary,
  QueryClient,
} from "@tanstack/react-query";
import ThePosts from "@/components/main/posts";
import type { Metadata } from "next";
import TheBio from "@/components/main/bio";

export default async function Home() {
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery({
    queryKey: ["posts"],
  });
  return (
    <HydrationBoundary state={dehydrate(queryClient)}>
      <TheBio />
      <ThePosts />
    </HydrationBoundary>
  );
}

export const metadata: Metadata = {
  title: "Home — Humberto López",
};
