import SinglePost from "@/components/main/singlepost";
import { fetchSinglePost } from "@/data/posts/postsQueries";
import { fetchMedia } from "@/data/media/mediaQueries";
import { QueryClient } from "@tanstack/react-query";

type Props = {
  params: { slug: string };
};

export default async function Section({ params }: Props) {
  return <SinglePost slug={params.slug}></SinglePost>;
}

export async function generateMetadata({ params }: Props) {
  const queryClient = new QueryClient();
  const post = await queryClient.fetchQuery({
    queryKey: ["post"],
    queryFn: () => fetchSinglePost(params.slug[0]),
  });
  const media = await queryClient.fetchQuery({
    queryKey: ["media"],
    queryFn: () => fetchMedia(post[0].featured_media),
  });
  const excerpt = post[0].excerpt.rendered;
  return {
    title: post[0].title.rendered,
    type: "article",
    publishedTime: post[0].date_gmt,
    authors: [{ name: "Humberto López" }],
    openGraph: {
      title: post[0].title.rendered,
      type: "article",
      publishedTime: post[0].date_gmt,
      authors: [{ name: "Humberto López" }],
      siteName: "Humberto López, fotógrafo",
      images: [{ url: media.source_url }],
      url: `${process.env.PROD_BASE_URL}/blog/${post[0].slug}`,
      description: excerpt.replace(/(<([^>]+)>)/gi, ""),
    },
  };
}
