import type { Metadata } from "next";
import ReactQueryProvider from "@/components/ReactQueryProvider";
import TheMenu from "@/components/main/menu";
import TheFooter from "@/components/main/footer";
import "./globals.css";
import { Poppins, Josefin_Sans, Dosis, Urbanist } from "next/font/google";
import { GoogleTagManager, GoogleAnalytics } from "@next/third-parties/google";

const poppins = Poppins({
  weight: ["400", "700"],
  style: ["normal"],
  subsets: ["latin"],
  display: "swap",
  variable: "--font-poppins",
});

const josefinsans = Josefin_Sans({
  weight: ["400", "700"],
  style: ["normal"],
  subsets: ["latin"],
  display: "swap",
  variable: "--font-josefinsans",
});

const dosis = Dosis({
  weight: ["400", "700"],
  style: ["normal"],
  subsets: ["latin"],
  display: "swap",
  variable: "--font-dosis",
});

const urbanist = Urbanist({
  weight: ["400", "500", "700"],
  style: ["normal"],
  subsets: ["latin"],
  display: "swap",
  variable: "--font-urbanist",
});

export const metadata: Metadata = {
  title: {
    template: "%s — Humberto López",
    default: "Humberto López",
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html
      lang="en"
      className={`${poppins.variable} ${josefinsans.variable} ${dosis.variable} ${urbanist.variable}`}
    >
      <GoogleTagManager gtmId="GTM-TNRZCD8Z" />
      <GoogleAnalytics gaId="G-MJC7Q1T557" />
      <body>
        <ReactQueryProvider>
          <TheMenu />
          {children}
          <TheFooter />
        </ReactQueryProvider>
      </body>
    </html>
  );
}
