"use client";

import { useParams } from "next/navigation";

export default function Year() {
  const params = useParams<{ year: string }>();
  const isYear = (year: string) =>
    year.length === 4 ? Number.isFinite(+year) : false;
  if (isYear(params.year))
    return <p>Esta es la página del año {params.year}</p>;
  return <p>404</p>;
}
