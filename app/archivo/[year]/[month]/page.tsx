"use client";

import { useParams } from "next/navigation";

export default function Month() {
  const params = useParams<{ month: string }>();
  const isMonth = (month: string) =>
    month.length === 2 ? Number.isFinite(+month) : false;
  if (isMonth(params.month))
    return <p>Esta es la página del año {params.month}</p>;
  return <p>404</p>;
}
