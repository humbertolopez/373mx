"use client";

import { useQuery } from "@tanstack/react-query";
import { fetchPostsFromCategory } from "@/data/posts/postsQueries";
import Link from "next/link";

const TheLinkInBioPosts = () => {
  const { data, isLoading } = useQuery({
    queryKey: ["posts"],
    queryFn: () => fetchPostsFromCategory(9),
  });

  const BlogHeading = () => (
    <div className="xl:container mx-auto mb-12 mt-4">
      <h1 className="text-3xl text-center urbanist font-medium uppercase">
        Humberto López
      </h1>
      <p className="text-xl text-center urbanist font-medium uppercase">
        Links in Bio
      </p>
    </div>
  );

  if (isLoading)
    return (
      <div className="w-full border-t p-10">
        <BlogHeading />
        <section
          id="home-page-posts-loading"
          className="xl:container mx-auto grid gap-8 xl:grid-cols-3 md:grid-cols-2 grid-cols-1 animate-pulse"
        >
          <div className="post-skeleton w-full h-60 bg-gray-200 rounded-lg"></div>
          <div className="post-skeleton w-full h-60 bg-gray-200 rounded-lg"></div>
          <div className="post-skeleton w-full h-60 bg-gray-200 rounded-lg"></div>
        </section>
      </div>
    );

  type Category = {
    id: number;
    name: string;
    slug: string;
  };

  type FeaturedMedia = {
    source_url: string;
  };

  type Author = {
    id: number;
    name: string;
    avatar_urls: {
      24: string;
      48: string;
    };
  };

  type SinglePostFromQuery = {
    id: number;
    slug: string;
    title: {
      rendered: string;
    };
    excerpt: {
      rendered: string;
    };
    _embedded: {
      ["wp:featuredmedia"]: FeaturedMedia[];
      ["wp:term"]: Category[];
      author: Author[];
    };
  };

  type SinglePostToRender = {
    id: number;
    title: string;
    excerpt: SinglePostFromQuery["excerpt"]["rendered"];
    author: string;
    categories: Category[];
    featuredImage: string;
    slug: string;
    authorAvatar: string;
  };

  const posts: Array<SinglePostToRender> = [];

  data.map((post: SinglePostFromQuery) => {
    if (Array.isArray(posts))
      posts.push({
        id: post.id,
        title: post.title.rendered,
        featuredImage: post._embedded["wp:featuredmedia"]
          ? post._embedded["wp:featuredmedia"][0].source_url
          : "",
        categories: post._embedded["wp:term"],
        author: post._embedded.author[0].name,
        authorAvatar: post._embedded.author[0].avatar_urls[24],
        excerpt: post.excerpt.rendered,
        slug: post.slug,
      });
  });

  return (
    <div className="w-full p-10">
      <BlogHeading />
      <section
        id="linkinbio"
        className="container mx-auto grid grid-cols-1 max-w-screen-sm"
      >
        {posts &&
          posts.map((post: SinglePostToRender) => (
            <article key={post.id} className="p-4 border-t">
              {post.featuredImage && (
                <div className="featured-image w-1/4 float-left">
                  <Link href={`/blog/${post.slug}`}>
                    <img src={post.featuredImage}></img>
                  </Link>
                </div>
              )}
              <div className="article-body w-3/4 pl-4 float-left">
                <h2 className="text-3xl font-medium m0 urbanist truncate">
                  <Link
                    href={`/blog/${post.slug}`}
                    className="underlined_hover"
                  >
                    {post.title}
                  </Link>
                </h2>
                <Link href={`/blog/${post.slug}`} className="underlined_hover">
                  Leer artículo completo →
                </Link>
              </div>
            </article>
          ))}
      </section>
    </div>
  );
};

export default TheLinkInBioPosts;
