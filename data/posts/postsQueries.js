function fetchPosts() {
  const data = fetch(`${process.env.api_url}/posts?_embed`).then((response) =>
    response.json()
  );
  return data;
}

function fetchSinglePost(slug) {
  const data = fetch(`${process.env.api_url}/posts?slug=${slug}`).then(
    (response) => response.json()
  );
  return data;
}

function fetchPostsFromCategory(catID) {
  const data = fetch(
    `${process.env.api_url}/posts?categories=${catID}&_embed`
  ).then((response) => response.json());
  return data;
}

export { fetchPosts, fetchSinglePost, fetchPostsFromCategory };
