function fetchSinglePage(params) {
  const data = fetch(`${process.env.api_url}/pages?slug=${params.slug}`).then(
    (response) => response.json()
  );
  return data;
}

export { fetchSinglePage };
