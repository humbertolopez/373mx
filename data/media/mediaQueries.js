function fetchMedia(mediaID) {
  const data = fetch(`${process.env.api_url}/media/${mediaID}?_embed`).then(
    (response) => response.json()
  );
  return data;
}

export { fetchMedia };
