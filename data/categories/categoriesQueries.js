"use client";

import { useQuery } from "@tanstack/react-query";

async function fetchCategories() {
  const data = await fetch("https://373.mx/wp-json/wp/v2/categories").then(
    (response) => response.json()
  );
  return data;
}

const TheCategories = (catsIds) => {
  const { data, isLoading } = useQuery({
    queryKey: ["categories"],
    queryFn: () => fetchCategories(),
  });

  function getPostCategoryName(catId) {
    const result = data.filter((category) => category.id === catId);
    const name = result[0].name;
    const url = result[0].link;
    return <a href={url}>{name}</a>;
  }

  if (isLoading) return <li>Loading...</li>;

  return (
    <ul>
      {catsIds &&
        catsIds.catsIds.map((cat) => (
          <li key={cat}>{getPostCategoryName(cat)}</li>
        ))}
    </ul>
  );
};

export default TheCategories;
