"use client";

async function fetchAuthor(authorId) {
  const data = await fetch(`${process.env.api_url}/users/${authorId}`).then(
    (response) => response.json()
  );
  return data;
}

export { fetchAuthor };
